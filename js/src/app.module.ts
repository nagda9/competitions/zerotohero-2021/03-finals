import { Module } from '@nestjs/common';
import { DontoController } from './donto/donto.controller';

@Module({
  imports: [],
  controllers: [DontoController],
  providers: [],
})
export class AppModule {}
