import { Body, Controller, Get, Post } from '@nestjs/common';
import { HammingDto } from "./dto/hamming.dto";
import { min } from "rxjs/operators";
import { PrimeDto } from "./dto/prime.dto";
import { StringToBinDto } from "./dto/stringToBin.dto";
import { SubNumDto } from "./dto/subNum.dto";
import { RobberyDto } from "./dto/robbery.dto";

@Controller()
export class DontoController {
	
	@Get('/ping')
	ping(): string {
		return 'pong';
	}
	
	@Post('/hamming')
	hamming(@Body() hamming: HammingDto) {
		let minDist = 8;
		for (let item1 of hamming.codeWords) {
			for (let item2 of hamming.codeWords) {
				if (item1 !== item2) {
					let dist = 0;
					for (let i = 0; i < 8; i++) {
						if (item1[i] !== item2[i]) {
							dist++;
						}
					}
					if (dist < minDist) {
						minDist = dist;
					}
				}
			}
		}
		return minDist;
	}
	
	@Post('/primeFactors')
	prime(@Body() num: PrimeDto) {
		let next = num.numberToFactorize
		let twos = 0
		while (true) {
			next = next / 2;
			twos++
			if (next % 2 !== 0) {
				break
			}
		}
		//console.log(twos)
		return twos
	}
	
	//@Post()
	stringToBinary(@Body() str: StringToBinDto) {
		console.log(parseInt(str.asciiString).toString(2))
	}
	
	//@Post('/maxSubNumber')
	subNum (@Body() num: SubNumDto) {
		let shift = 0
		let ans = "";
		let answers = []

		while (shift < 9) {
			for (let i = 0; i <= shift; i++) {
				let ans = num.numberToCheck.toString()[shift]
				let last = parseInt(num.numberToCheck.toString()[shift])

				for (let j = 0; j < 10-shift; j++) {
					let current = parseInt(num.numberToCheck.toString()[shift+j]);
					if(current > last) {
						last = current
						ans += num.numberToCheck.toString()[j]
					} else {
						break
					}
					if (j === 10-shift-1) {
						answers.push(ans)
					}
				}
			}
			shift++;
		}
		console.log(answers)
	}
	
	//@Post("/robbery")
	robbery (@Body() rob: RobberyDto) {
	
	}
}
