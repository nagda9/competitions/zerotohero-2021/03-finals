import { Test, TestingModule } from '@nestjs/testing';
import { DontoController } from './donto.controller';

describe('donto Controller', () => {
  let controller: DontoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DontoController],
    }).compile();

    controller = module.get<DontoController>(DontoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  
  it('hamming', () => {
    controller.hamming({
      codeWords: ['00111011', '00111010']
    })
    
  })
  
  it('hamming2', () => {
    controller.hamming({
      codeWords: ['00111011', '00111000', '00000000']
    })
  })
  
  it('prime1', () => {
    controller.prime({
        numberToFactorize: 19832
    })
  })
  
  it("prime2", () => {
    controller.prime({
        numberToFactorize: 100
      }
    )
  })
  
  it("prime3", () => {
    controller.prime({
        numberToFactorize: 256
      }
    )
  })
  
  it("str2bin", () => {
    controller.stringToBinary({
        asciiString: "Abt"
      }
    )
  })
  
  it("maxSub", () => {
    controller.subNum({
        numberToCheck: 5416872569
      }
    )
  })
  
  it("robbery", () => {
    controller.robbery({
      houses: [3,2,3,null,3,null,1]
    })
  })
});
