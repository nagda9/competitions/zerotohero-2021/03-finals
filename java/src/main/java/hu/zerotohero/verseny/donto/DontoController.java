package hu.zerotohero.verseny.donto;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DontoController {

    @GetMapping("/ping")
    public String ping() {
        return "pong";
    }
}
